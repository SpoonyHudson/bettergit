﻿using System.Collections.Generic;
using UnityEngine;
namespace Hudson.Shaders.Outline
{
    public class OutlineHelper : MonoBehaviour
    {

        // Use this for initialization
        void Awake()
        {

            Mesh coremesh;
            if (GetComponent<SkinnedMeshRenderer>() != null)
                coremesh = GetComponent<SkinnedMeshRenderer>().sharedMesh;
            else
                coremesh = GetComponent<MeshFilter>().mesh;
            Vector3[] verts = coremesh.vertices;
            Vector3[] normals = coremesh.normals;
            Dictionary<Vector3, Vector3> newnormals = new Dictionary<Vector3, Vector3>();

            for (int i = 0; i < verts.Length; i++)
            {
                if (newnormals.ContainsKey(verts[i]))
                    newnormals[verts[i]] += normals[i];
                else
                    newnormals.Add(verts[i], normals[i]);
            }
            for (int i = 0; i < verts.Length; i++)
            {
                normals[i] = newnormals[verts[i]].normalized;
            }
            coremesh.normals = normals;
            if (GetComponent<SkinnedMeshRenderer>() != null)
                GetComponent<SkinnedMeshRenderer>().sharedMesh = coremesh;
            else
                GetComponent<MeshFilter>().mesh = coremesh;
        }

    }
}
