﻿// Upgrade NOTE: upgraded instancing buffer 'Props' to new syntax.


Shader "Custom/outline" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_OutlineColor("Outline Color", Color) = (1,1,1,1)		
		_StencilMask("Stencil Mask", Range(1,224)) = 1
		_Amount("amount", Range(-2,2)) = 1.5
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_DotForShadow("Dot For Shadow",range(-1,1)) = 0
		_ShadowAmount("Shadow Amount",range(0,1)) = 0.5

	}
		SubShader{

			Tags { "RenderType" = "Opaque" }
			LOD 200
			Stencil{
			Ref [_StencilMask]
			Comp Always
			Pass Replace
			}
			CGPROGRAM
			// CellLighting style
			#pragma surface surf SimpleLambert
			float _DotForShadow;

		float _ShadowAmount;
			half4 LightingSimpleLambert(inout SurfaceOutput s, half3 lightDir, half atten) {
			float ndl = dot(s.Normal, lightDir)*atten;

			half4 c;
			c.rgb = float4(0, 0, 0, 0);
			if (ndl >= _DotForShadow)
				c.rgb = float4(0,0,0,0);
			else
			{
				if (_DotForShadow < 0)
				{
					if (ndl >= _DotForShadow - (-_DotForShadow*0.5))
					{
						s.Emission = s.Emission*((_ShadowAmount + 1)*0.5);
					}
					else
						s.Emission = s.Emission*_ShadowAmount;
				}
				else if (_DotForShadow == 0)
				{
					if (ndl >= -0.5)
					{
						s.Emission = s.Emission*((_ShadowAmount+1)*0.5);
					}
					else
						s.Emission = s.Emission*_ShadowAmount;
				}
				else
				{
					if (ndl >= _DotForShadow - (_DotForShadow*0.5))
					{
						s.Emission = s.Emission*((_ShadowAmount + 1)*0.5);
					}
					else
						s.Emission = s.Emission*_ShadowAmount;
				}
				//s.Emission = s.Emission*_ShadowAmount;
			}
			c.a = s.Alpha;
			return c;
		}

			// Use shader model 3.0 target, to get nicer looking lighting
			#pragma target 3.0
			sampler2D _MainTex;

			struct Input {
				float2 uv_MainTex;
			};

			
			fixed4 _Color;
			// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
			// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
			// #pragma instancing_options assumeuniformscaling
			UNITY_INSTANCING_BUFFER_START(Props)
				// put more per-instance properties here
			UNITY_INSTANCING_BUFFER_END(Props)

			void surf(Input IN, inout SurfaceOutput o) {
				// Albedo comes from a texture tinted by color
				fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
				o.Emission = c.rgb;
				
				
				o.Alpha = c.a;
			}
			ENDCG
			
			Pass
			{
				Stencil
				{
					ref[_StencilMask]
					Comp NotEqual
				}
				Name "Outline"
				ZWrite On
				Cull Off
				CGPROGRAM
	#pragma vertex vert
	#pragma fragment frag
	#pragma	multi_compile_fog
	#include "UnityCG.cginc"

				struct appdata
				{
					float4 vertex : POSITION;
					float2 uv : TEXCOORD0;
				};

				struct v2f
				{
					UNITY_FOG_COORDS(1)
					float4 vertex : SV_POSITION;
				};

				float4 _OutlineColor;
				float _Amount;
		
				v2f vert(appdata_full v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex+(v.normal*_Amount));
					UNITY_TRANSFER_FOG(o,o.vertex);
					return o;
				}
				fixed4 frag(v2f i) : SV_Target
				{
					return  _OutlineColor;
				}
				ENDCG
			}
		}
}
