using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(AnimeMover))]
public class AnimeMoverEditor : Editor
{
   

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        AnimeMover obj = (AnimeMover)target;

        if (GUILayout.Button("Mark Children"))
        {
            obj.matchPair();
        }

       

    }
}
