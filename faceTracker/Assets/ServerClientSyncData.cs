using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class ServerClientSyncData : NetworkBehaviour
{

    [SyncVar] Quaternion HeadRotation;
    [SyncVar] Vector3 MouthL;
    [SyncVar] Vector3 MouthR;
    [SyncVar] Vector3 MouthT;
    [SyncVar] Vector3 MouthB;
    [SyncVar] Vector3 EyeL;
    [SyncVar] Vector3 EyeR;
    [SyncVar] Vector3 BrowL;
    [SyncVar] Vector3 BrowR;
    [SyncVar] float gametime;


    public float getTime()
    {
        return gametime;
    }
    private void Start()
    {
        gametime = 0;
    }
    // Update is called once per frame
    void Update()
    {
        timeUpdate();
    }
    [ServerCallback] void timeUpdate()
    {
        gametime += Time.deltaTime;
    }
}
