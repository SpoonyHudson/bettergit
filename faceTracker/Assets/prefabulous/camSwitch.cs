using System.Collections;
using System.Collections.Generic;
using UnityEngine.Serialization;
using UnityEngine.XR.ARFoundation;
using UnityEngine;
using DG.Tweening;
using System.IO;
public class camSwitch : MonoBehaviour
{
    public float OPEN = 100;
    public float CLOSE = 0;
    public float LbrowC = 0;
    public float LbrowO = 100;
    public float RBC = 0;
    public float RBO = 100;
    public float mouthOOpen = 0;
    public float mouthOClosed = 100;
    public float HappyAmount = 0;
    public float sadAmount = 0;
    public float angryAmount = 0;
    public float timeDilation =0;
    public Vector3 browPointL;
    public Vector3 browPointR;
    public bool Calibration;
    public bool Calibrated;
    bool happytoggle;
    [SerializeField] Client C;
    public mouthSounds mouthSounds;

    void Start()
    {
        Calibrated = false;
        happytoggle = false;
        mouthSounds.A = new MouthPose();
        mouthSounds.E = new MouthPose();
        mouthSounds.U = new MouthPose();
        mouthSounds.I = new MouthPose();
        mouthSounds.O = new MouthPose();
        
    }

    void loadPref()
    {
        Calibrated = true;
        loadjson();
        OPEN = PlayerPrefs.GetFloat("OPEN", OPEN);
        CLOSE = PlayerPrefs.GetFloat("Closed", CLOSE);
        mouthOOpen = PlayerPrefs.GetFloat("MOO", mouthOOpen);
        mouthOClosed = PlayerPrefs.GetFloat("MOC", mouthOClosed);
        LbrowC = PlayerPrefs.GetFloat("LBC", LbrowC);
        LbrowO = PlayerPrefs.GetFloat("LBO", LbrowO);
        RBC = PlayerPrefs.GetFloat("RBC", RBC);
        RBO = PlayerPrefs.GetFloat("RBO", RBO);
    }

    public void loadjson()
    {
        mouthSounds c = JsonUtility.FromJson<mouthSounds>(File.ReadAllText(Application.persistentDataPath + "/SavedMouthSounds.json"));
        mouthSounds = c;
        


    }

    public void savetoJson() 
    { string json = JsonUtility.ToJson(mouthSounds);
        File.WriteAllText(Application.persistentDataPath + "/SavedMouthSounds.json", json);
    }
    void savePref()
    {
        savetoJson();
        PlayerPrefs.SetFloat("OPEN", OPEN);
        PlayerPrefs.SetFloat("Closed", CLOSE);
        PlayerPrefs.SetFloat("MOO", mouthOOpen);
        PlayerPrefs.SetFloat("MOC", mouthOClosed);
        PlayerPrefs.SetFloat("LBC", LbrowC);
        PlayerPrefs.SetFloat("RBO", RBO);
        PlayerPrefs.SetFloat("RBC", RBC);
        PlayerPrefs.SetFloat("LBO", LbrowO);
        Calibrated = true;
    }

#if UNITY_ANDROID
    void OnGUI()
    {
        if (GUI.Button(new Rect(50, 0, 100, 100), "Calibrate"))
        {
            if (Calibration)
            {
                Calibration = false;
            }
            else
            {
                Calibration = true;
            }
        }
        if (Calibration)
        {
            if (GUI.Button(new Rect(50, 200, 100, 100), "Mouth OPEN"))
            {
                calibrateOpen();

            }
            if (GUI.Button(new Rect(200, 200, 100, 100), "Mouth closed"))
            {

                calibrateClosed();
            }

            if (GUI.Button(new Rect(400, 200, 100, 100), "EYS OPEN"))
            {
                calibrateBrowsOpen();

            }
            if (GUI.Button(new Rect(500, 200, 100, 100), "EYES closed"))
            {

                calibrateBrowsClosed();
            }

            if (GUI.Button(new Rect(200, 350, 100, 100), "O closed"))
            {

                calibrateOClosed();
            }
            if (GUI.Button(new Rect(50, 350, 100, 100), "O Open"))
            {
                calibrateOOpen();
            }
            if (GUI.Button(new Rect(50, 450, 100, 100), "save"))
            {
                savePref();
            }
            if (GUI.Button(new Rect(200, 450, 100, 100), "load"))
            {
                loadPref();
            }
        }
        else
        {
            HappyAmount = GUI.HorizontalSlider(new Rect(50, 200, 400, 150), HappyAmount, 0, 100);
            sadAmount = GUI.HorizontalSlider(new Rect(50, 400, 400, 150), sadAmount, 0, 100);
            angryAmount = GUI.HorizontalSlider(new Rect(50, 600, 400, 150), angryAmount, 0, 100);
        }

    }
    public void calibrateOpen()
    {
        ARFace face = FindObjectOfType<MousePoints>().GetComponent<ARFace>();
        OPEN = Vector3.Distance(face.vertices[12], face.vertices[14]);
    }
    public void calibrateClosed()
    {
        ARFace face = FindObjectOfType<MousePoints>().GetComponent<ARFace>();
        CLOSE = Vector3.Distance(face.vertices[12], face.vertices[14]);
    }

    public void calibrateBrowsOpen()
    {
        ARFace face = FindObjectOfType<MousePoints>().GetComponent<ARFace>();
        LbrowO = Vector3.Distance(face.vertices[374], face.vertices[334]);
        RBO = Vector3.Distance(face.vertices[145], face.vertices[107]);
    }
    public void calibrateBrowsClosed()
    {
        ARFace face = FindObjectOfType<MousePoints>().GetComponent<ARFace>();
        LbrowC = Vector3.Distance(face.vertices[374], face.vertices[334]);
        RBC = Vector3.Distance(face.vertices[145], face.vertices[107]);
    }
    public void calibrateOClosed()
    {
        ARFace face = FindObjectOfType<MousePoints>().GetComponent<ARFace>();
        mouthOClosed = Vector3.Distance(face.vertices[78], face.vertices[292]);
    }
    public void calibrateOOpen()
    {
        ARFace face = FindObjectOfType<MousePoints>().GetComponent<ARFace>();
        mouthOOpen = Vector3.Distance(face.vertices[78], face.vertices[292]);
    }
#else

    void OnGUI()
    {
        if (GUI.Button(new Rect(50, 0, 100, 100), "Calibrate"))
        {
            if (Calibration)
            {
                Calibration = false;
            }
            else
            {
                Calibration = true;
            }
        }
        if (Calibration)
        {
            if (GUI.Button(new Rect(50, 200, 100, 100), "A"))
            {
                calibrateOpen(C.lastUpdate);

            }
            if (GUI.Button(new Rect(200, 200, 100, 100), "Mouth closed"))
            {

                calibrateClosed(C.lastUpdate);
            }

            if (GUI.Button(new Rect(400, 200, 100, 100), "FACE tall"))
            {
                LbrowC = Vector3.Distance(C.lastUpdate.CHIN, C.lastUpdate.Head);

            }
            if (GUI.Button(new Rect(500, 200, 100, 100), "U"))
            {

                calibrateU(C.lastUpdate);
            }

            if (GUI.Button(new Rect(600, 200, 100, 100), "FACE short"))
            {
                LbrowO = Vector3.Distance(C.lastUpdate.CHIN, C.lastUpdate.Head);

            }
            if (GUI.Button(new Rect(200, 350, 100, 100), "E"))
            {

                calibrateE(C.lastUpdate);
            }
            if (GUI.Button(new Rect(50, 350, 100, 100), "O"))
            {
                calibrateOOpen(C.lastUpdate);
            }
            if (GUI.Button(new Rect(50, 450, 100, 100), "save"))
            {
                savePref();
            }
            if (GUI.Button(new Rect(200, 450, 100, 100), "load"))
            {
                loadPref();
            }
        }
       

    }
    public void calibrateOpen(NetworkFacePacket P)
    {
        OPEN = Vector3.Distance(P.MouthB, P.MouthT);
        mouthSounds.A.registerOpen(P.MouthPoints);
    }
    public void calibrateClosed(NetworkFacePacket P)
    {
        CLOSE = Vector3.Distance(P.MouthB, P.MouthT);
        mouthSounds.A.RegisterClosed(P.MouthPoints);
        mouthSounds.O.RegisterClosed(P.MouthPoints);
        mouthSounds.E.RegisterClosed(P.MouthPoints);
        mouthSounds.I.RegisterClosed(P.MouthPoints);
        mouthSounds.U.RegisterClosed(P.MouthPoints);
    }

    public void calibrateU(NetworkFacePacket P)
    {
        mouthSounds.U.registerOpen(P.MouthPoints);
    }
    public void calibrateI(NetworkFacePacket P)
    {
        mouthSounds.I.registerOpen(P.MouthPoints);
    }
    public void calibrateE(NetworkFacePacket P)
    {
        mouthSounds.E.registerOpen(P.MouthPoints);
    }
    public void calibrateOOpen(NetworkFacePacket P)
    {
        mouthSounds.O.registerOpen(P.MouthPoints);
    }
#endif
}
