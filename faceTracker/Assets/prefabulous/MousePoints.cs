using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.XR.ARFoundation;
#if UNITY_ANDROID
using UnityEngine.XR.ARCore;
#endif
//1: top lip, 14: bottom lip, 78: left mouth corner, 292: right mouth corner Lbrow 27-145  Rbrow 374-257
//Vector3(-0.072908774,5.3290704e-17,-1.4109362e-17)
public class MousePoints : MonoBehaviour
{
#if UNITY_ANDROID
    [SerializeField] ARFace face = null;
    [FormerlySerializedAs("indices")] [SerializeField] int[] mousePointsIndices = new[] { 1, 14, 78, 12, };
    [SerializeField] float pointerScale = 0.01f;
    [CanBeNull] [SerializeField] GameObject optionalPointerPrefab = null;

    Server server;

    AnimeMover animegirl;
    camSwitch camSwitch;
    readonly Dictionary<int, Transform> pointers = new Dictionary<int, Transform>();
    const int LEFTEYE = 374;
    const int RIGHTEYE = 145;
    float emotionTime = 50;
    Vector3[] moutpoints;
    
    void Awake()
    {
        moutpoints = new Vector3[14];
        animegirl = FindObjectOfType<AnimeMover>();
        // faceBlends = animegirl.FaceSkin;
        //  BrowBlends = animegirl.BrowSkin;
        camSwitch = FindObjectOfType<camSwitch>();
        server = FindObjectOfType<Server>();
        face.updated += delegate
        {
            //for (var i = 0; i < mousePointsIndices.Length; i++)
            //{
            //    var vertexIndex = mousePointsIndices[i];
            //    var pointer = getPointer(i);

            //    pointer.position = face.transform.TransformPoint(face.vertices[vertexIndex]);

            //}

            //animegirl.head.rotation = Quaternion.Euler(-face.transform.rotation.eulerAngles.x, face.transform.rotation.eulerAngles.y, face.transform.rotation.eulerAngles.z);

            //animegirl.mouthOpen.skin.SetBlendShapeWeight(animegirl.mouthOpen.ID, Mathf.Lerp(0, 100, Mathf.InverseLerp(camSwitch.CLOSE, camSwitch.OPEN, Vector3.Distance(face.vertices[12], face.vertices[14]))));

            //animegirl.LeftBrow.skin.SetBlendShapeWeight(animegirl.LeftBrow.ID, Mathf.Lerp(0, 70, Mathf.InverseLerp(camSwitch.LbrowC, camSwitch.LbrowO, Vector3.Distance(face.vertices[LEFTEYE], face.vertices[334]))));

            //animegirl.RightBrow.skin.SetBlendShapeWeight(animegirl.RightBrow.ID, Mathf.Lerp(0, 70, Mathf.InverseLerp(camSwitch.RBC, camSwitch.RBO, Vector3.Distance(face.vertices[RIGHTEYE], face.vertices[107]))));

            //animegirl.mouthO.skin.SetBlendShapeWeight(animegirl.mouthO.ID, Mathf.Lerp(0, 100, Mathf.InverseLerp(camSwitch.mouthOClosed, camSwitch.mouthOOpen, Vector3.Distance(face.vertices[78], face.vertices[292]))));
            NetworkFacePacket packet = new NetworkFacePacket();
            packet.MouthR = face.vertices[78];
            packet.MouthL = face.vertices[292];
            packet.MouthT = face.vertices[12];
            packet.MouthB = face.vertices[14];
            packet.EYEL = face.vertices[LEFTEYE];
            packet.EYER = face.vertices[RIGHTEYE];
            packet.BrowL = face.vertices[334];
            packet.BrowR = face.vertices[107];
            packet.HeadRotatonEulerAngles = face.transform.eulerAngles;
            packet.headPos = face.transform.position;
            packet.CHIN = face.vertices[175];
            packet.Head = face.vertices[10];

            //80,81,82,  13, 312,311,310
            //88,178,87, 14 ,317,402,318
            moutpoints[0] = face.vertices[80];
            moutpoints[1] = face.vertices[81];
            moutpoints[2] = face.vertices[82];
            moutpoints[3] = face.vertices[13];
            moutpoints[4] = face.vertices[312];
            moutpoints[5] = face.vertices[311];
            moutpoints[6] = face.vertices[310];
            moutpoints[7] = face.vertices[88];
            moutpoints[8] = face.vertices[178];
            moutpoints[9] = face.vertices[87];
            moutpoints[10] = face.vertices[14];
            moutpoints[11] = face.vertices[317];
            moutpoints[12] = face.vertices[402];
            moutpoints[13] = face.vertices[318];
            packet.MouthPoints = moutpoints;

            packet.LastFrameTime = Time.time;
            server.sendData(packet);
          

            //blendEmotion(animegirl.Smile.skin, animegirl.Smile.ID, camSwitch.HappyAmount);
            //blendEmotion(animegirl.Angry.skin, animegirl.Angry.ID, camSwitch.angryAmount);
            //blendEmotion(animegirl.Sad.skin, animegirl.Sad.ID, camSwitch.sadAmount);

        };



    }


    void blendEmotion(SkinnedMeshRenderer skin, int id, float target)
    {
        if (skin.GetBlendShapeWeight(id) != target)
            skin.SetBlendShapeWeight(id, Mathf.MoveTowards(skin.GetBlendShapeWeight(id), target, Time.deltaTime * emotionTime));
    }

    Transform getPointer(int id)
    {
        if (pointers.TryGetValue(id, out var existing))
        {
            return existing;
        }
        else
        {
            var newPointer = createNewPointer();
            pointers[id] = newPointer;
            return newPointer;
        }
    }

    Transform createNewPointer()
    {
        var result = instantiatePointer();
        return result;
    }

    Transform instantiatePointer()
    {
        if (optionalPointerPrefab != null)
        {
            return Instantiate(optionalPointerPrefab).transform;
        }
        else
        {
            var result = GameObject.CreatePrimitive(PrimitiveType.Sphere).transform;
            result.localScale = Vector3.one * pointerScale;
            return result;
        }
    }

#endif
}