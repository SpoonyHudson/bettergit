using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using DG.Tweening;
//hold things. get tied to face.

//doto split head rotation into head and neck
//totween implementaion;
//random blinks.
[Serializable]
public struct BlendPair

{
    public int ID;
    public string name;
    public SkinnedMeshRenderer skin;
    public float cap;
    public float min;
    public float curveMIN;
    public float curveRange;
    private float _amount;
    public float amount
    {
        get { return _amount; }
        set
        {


            _amount = ((value - (curveMIN)) * (cap - min) / curveRange) + min;

            skin.SetBlendShapeWeight(ID, _amount);

        }
    }
    public void matchPair()
    {
        Debug.Log("matching");
        SkinnedMeshRenderer head = skin;
        Mesh m = head.sharedMesh;

        for (int i = 0; i < m.blendShapeCount; i++)
        {
            string s = m.GetBlendShapeName(i);
            if (s == name)
            {
                ID = i;
                Debug.Log("Blend Shape: " + i + " " + name);
            }
            Debug.Log("Blend Shape: " + i + " " + s);
        }


    }

}
public class AnimeMover : MonoBehaviour
{
   public void matchPair()
    {
        mouthO.matchPair();
        mouthI.matchPair();
        LeftBrow.matchPair();
        _blink.matchPair();
        happy.matchPair();
        angry.matchPair();
        suprise.matchPair();
    }

    [System.Serializable]
    public class Emotion
    {
        public void matchPair()
        {
            for (int i = 0; i < pairs.Length; i++)
            {
                pairs[i].matchPair();
            }
        }
        public BlendPair[] pairs;
       public bool toggled;

        public void recursiveCallMIN(int i, Sequence seq )
        {
            seq.Join(DOTween.To(() => pairs[i].amount, x => pairs[i].amount = x, pairs[i].min, 0.25f));
        }
        public void recursiveCallMAX(int i, Sequence seq)
        {
            seq.Join(DOTween.To(() => pairs[i].amount, x => pairs[i].amount = x, pairs[i].cap, 0.25f));
        }
        public void Off()
        {
            if (toggled)
            {
                Sequence seq = DOTween.Sequence();

                for (int i = 0; i < pairs.Length; i++)
                {
                    recursiveCallMIN(i, seq);
                }
                //seq.Join(DOTween.To(() => pairs[0].amount, x => pairs[0].amount = x, pairs[0].min, 0.25f));
                //seq.Join(DOTween.To(() => pairs[1].amount, x => pairs[1].amount = x, pairs[1].min, 0.25f));
                //seq.Join(DOTween.To(() => pairs[2].amount, x => pairs[2].amount = x, pairs[2].min, 0.25f));



                seq.Play();
                toggled = false;
            }
        }
        public void on()
        { 
            if (!toggled)
            {
                Sequence seq = DOTween.Sequence();

                for (int i = 0; i < pairs.Length; i++)
                {
                    recursiveCallMAX(i, seq);
                }



                seq.Play();
                toggled = true;
            }
        }
    }

    [SerializeField] public Transform root;
    public Vector3 startPos;
    [SerializeField] public Transform head;
    [SerializeField] public Transform Neck;
    [SerializeField] public float neckLimit;

    [SerializeField] public BlendPair mouthO;
    [SerializeField] public BlendPair mouthI;
    
    [SerializeField] public BlendPair LeftBrow;
    [SerializeField] public BlendPair RightBrow;

    [SerializeField] public BlendPair _blink;
    [SerializeField] public camSwitch Switch;
    [SerializeField] public Emotion happy;
    [SerializeField] public Emotion angry;
    [SerializeField] public Emotion suprise;

    
    Sequence blink;
    Sequence flap;
    bool happytoggle;
    Sequence seq;
    private void Start()
    {
        startPos = root.transform.position;
        happytoggle = false;
        blink = DOTween.Sequence();
        blink.Append(DOTween.To(() => _blink.amount, x => _blink.amount = x, 100, 0.45f));
        blink.Append(DOTween.To(() => _blink.amount, x => _blink.amount = x, 0, 0.25f));
        blink.SetAutoKill(false);
        doBlink();
        //flap = DOTween.Sequence();
        //flap.Append(DOTween.To(() => LeftBrow.amount, x => LeftBrow.amount = x, 100f, 0.35f));
        //flap.Join(DOTween.To(() => RightBrow.amount, x => RightBrow.amount = x, 100f, 0.35f));
        //flap.Append(DOTween.To(() => RightBrow.amount, x => RightBrow.amount = x, 0, 0.35f));
        //flap.Join(DOTween.To(() => LeftBrow.amount, x => LeftBrow.amount = x, 0, 0.35f));
        //flap.SetAutoKill(false);
        //doflap();
    }
    private void Update()
    {
        
    }

    void doBlink()
    {

       
        blink.Restart();
       // blink.Play();
        Invoke("doBlink", UnityEngine.Random.Range(2f, 10f));
    }
    void doflap()
    {
        flap.Restart();
        Invoke("doflap", UnityEngine.Random.Range(10f, 27f));
    }
    private void OnGUI()
    {
        if (!Switch.Calibration)
        {
            if (GUI.Button(new Rect(200, 0, 100, 100), "Smile"))
            {
                if (happy.toggled)
                {
                    happy.Off();
                    
                    //DOTween.To(() => Smile.amount, x => Smile.amount = x, 0, 0.25f);
                    //happytoggle = false;
                }
                else
                {
                     happy.on();
                    
                    
                    happy.toggled = true;
                    angry.Off();
                    suprise.Off();
                   // DOTween.To(() => Smile.amount, x => Smile.amount = x, 64, 0.25f);
                  // happytoggle = true;
                }
                //  HappyAmount = 60;
                //    HappyAmount = 0;
            }
            if (GUI.Button(new Rect(400, 0, 100, 100), "angrey"))
            {
                if (angry.toggled)
                {
                    angry.Off();
                    //DOTween.To(() => Smile.amount, x => Smile.amount = x, 0, 0.25f);
                    //happytoggle = false;
                }
                else
                {
                    angry.on();
                    happy.Off();
                    suprise.Off();
                    // DOTween.To(() => Smile.amount, x => Smile.amount = x, 64, 0.25f);
                    // happytoggle = true;
                }
                //  HappyAmount = 60;
                //    HappyAmount = 0;
            }
            if (GUI.Button(new Rect(600, 0, 100, 100), "SHOCK"))
            {
                if (suprise.toggled)
                {
                    suprise.Off();
                    //DOTween.To(() => Smile.amount, x => Smile.amount = x, 0, 0.25f);
                    //happytoggle = false;
                }
                else
                {
                    suprise.on();
                    happy.Off();
                    angry.Off();
                    // DOTween.To(() => Smile.amount, x => Smile.amount = x, 64, 0.25f);
                    // happytoggle = true;
                }
                //  HappyAmount = 60;
                //    HappyAmount = 0;
            }
        }
     //   sadAmount = GUI.HorizontalSlider(new Rect(50, 400, 400, 150), sadAmount, 0, 100);
     //   angryAmount = GUI.HorizontalSlider(new Rect(50, 600, 400, 150), angryAmount, 0, 100);
    }
}
