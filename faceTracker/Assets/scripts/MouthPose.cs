using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class MouthPose 
{

    public Vector3[] closedPoints = new Vector3[14]; //in order 
                                              //TOP:    80,81,82,  13, 312,311,310
                                              //Bottom: 88,178,87, 14 ,317,402,318
    public Vector3[] openPoints = new Vector3[14];
    float[] distance = new float[14];
    float xValueC;
    float yValueC;

    public float calcX(Vector3[] other)
    {
        float x = Vector3.Distance(other[0], other[6]);
        float y = Vector3.Distance(other[8], other[13]);
        return (x + y) / 2f;       
    }

    public float calcY(Vector3[] other)
    {
        float x = 0;
        for (int i = 0; i < 6; i++)
        {
            x += Vector3.Distance(other[i], other[i + 7]);
        }

        return x/7f;
    }

    public float Ysim(Vector3[] other)
    {
        return calcY(other) - yValueC;
    }

    public float Xsim(Vector3[] other)
    {
        return calcX(other) - xValueC;
    }

    public void RegisterClosed(Vector3[] other)
    {

        other.CopyTo(closedPoints,0);
        xValueC= calcX(other);
        yValueC = calcY(other);
        
       
    }
    public void registerOpen(Vector3[] other)
    {
        other.CopyTo(openPoints, 0);
    }

   public float simularity(Vector3[] other)
    {
        
        float c=0;
        for (int i = 0; i < 14; i++)
        {
            c += InverseLerp(closedPoints[i], openPoints[i], other[i]);
          
        }
        return (c/14f);
    }
    public float InverseLerp(Vector3 a, Vector3 b, Vector3 value)
    {
        Vector3 AB = b - a;
        Vector3 AV = value - a;
        return Mathf.Clamp( Vector3.Dot(AV, AB) / Vector3.Dot(AB, AB),0f,1f);
    }
}


[System.Serializable]
public class mouthSounds
{
    public MouthPose A;
    public MouthPose E;
    public MouthPose I;
    public MouthPose O;
    public MouthPose U;
}
