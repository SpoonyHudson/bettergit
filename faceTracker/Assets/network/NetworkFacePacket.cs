using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LiteNetLib;
using LiteNetLib.Utils;

public class NetworkFacePacket 
{

    public Vector3 EYEL { get; set; }
    public Vector3 EYER  {get; set; }
    public Vector3 BrowL { get; set; }
    public Vector3 BrowR { get; set; }
    public Vector3 MouthT { get; set; }
    public Vector3 MouthB { get; set; }
    public Vector3 MouthL { get; set; }
    public Vector3 MouthR { get; set; }
    public Vector3 HeadRotatonEulerAngles { get; set; }

    public Vector3 headPos { get; set; }
    public Vector3 fixationPoint { get; set; }
    public Vector3[] MouthPoints { get; set; }
    public Vector3 CHIN { get; set; }
    public Vector3 Head { get; set; }

    public float LastFrameTime { get; set; }

    public NetworkFacePacket(NetworkFacePacket o)
    {
        EYEL = o.EYEL;
        EYER = o.EYER;
        BrowL = o.BrowL;
        BrowR = o.BrowR;
        MouthT = o.MouthT;
        MouthB = o.MouthB;
        MouthL = o.MouthL;
        MouthR = o.MouthR;
        HeadRotatonEulerAngles = o.HeadRotatonEulerAngles;
        MouthPoints = o.MouthPoints;
        headPos = o.headPos;
        fixationPoint = o.fixationPoint;
        CHIN = o.CHIN;
        Head = o.Head;
        LastFrameTime = o.LastFrameTime;
    }
    public NetworkFacePacket()
    {
        EYEL = 
        EYER =
        BrowL =
        BrowR = 
        MouthT = 
        MouthB = 
        MouthL = 
        MouthR = 
        HeadRotatonEulerAngles = Vector3.zero;
        LastFrameTime = 0;
    }

  
}

public class Vector3Utils {
    public static void Serialize(NetDataWriter writer, Vector3 vector)
    {
        writer.Put(vector.x);
        writer.Put(vector.y);
        writer.Put(vector.z);
    }
    public static void Serialize(NetDataWriter writer, Vector3[] vectorA)
    {
        foreach (Vector3 v in vectorA)
            Serialize(writer, v);
    }
    public static Vector3 Deserialize(NetDataReader reader)
    {
        return new Vector3(reader.GetFloat(), reader.GetFloat(), reader.GetFloat());
    }
}

