using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using LiteNetLib;
using LiteNetLib.Utils;
public class Server : MonoBehaviour
{

    NetManager netManager;
    EventBasedNetListener netListener;
    NetPacketProcessor netProcessor;
    public NetworkFacePacket facePacket;

    [SerializeField] Text text;
    float updatecount= 0;
    // Start is called before the first frame update
    void Start()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
            netListener = new EventBasedNetListener();
        netManager = new NetManager(netListener);
        netProcessor = new NetPacketProcessor();
       // facePacket = new NetworkFacePacket();
        netProcessor.RegisterNestedType(Vector3Utils.Serialize, Vector3Utils.Deserialize);
        netManager.Start(9050);
        netListener.ConnectionRequestEvent += (request) => {
            request.Accept();
        };

        netListener.PeerConnectedEvent += (client) => {
            
        };
        
        
    }

    public void sendData(NetworkFacePacket packet)
    {

        facePacket = packet;
       
    }



    // Update is called once per frame
    void Update()
    {
        if(facePacket !=null)
         netProcessor.Send(netManager, (facePacket), DeliveryMethod.Unreliable);
        netManager.PollEvents();
      
    }


}



