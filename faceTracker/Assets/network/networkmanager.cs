using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
public class networkmanager : MonoBehaviour
{
    string ip= "192.168.0.34";
    [SerializeField] Client client;
    [SerializeField] Text conntectionStatus;
    bool display = true;
    // Start is called before the first frame update
    void Start()
    {
        display = true;   
    }

    private void OnGUI()
    {
        if (display)
        {
            ip = GUI.TextArea(new Rect(0, 0, 500, 200), ip);
            if (GUI.Button(new Rect(0, 300, 100, 100), "connect"))
            {
                client.ConnextToServer(ip);
                display = false;
            }
        }
    }

    public void Connected()
    {
        
        conntectionStatus.text = "connected";
    }

    // Update is called once per frame
    void Update()
    {
       
    }
}
