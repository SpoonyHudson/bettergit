using System;
using UnityEngine;
using UnityEngine.Networking;
using LiteNetLib;
using LiteNetLib.Utils;
public class Client : MonoBehaviour
{
    NetManager netManager;
    EventBasedNetListener netListener;
    NetPacketProcessor netPacketProcessor; // New field

    [SerializeField] AnimeMover animegirl;
    [SerializeField]camSwitch camSwitch;
    public NetworkFacePacket lastUpdate;
    float LastTimeArived;
    public float time { get { return timeArived-LastTimeArived; } }
    float timeArived;
    string ip ="192.168.0.34";
    private void Start()
    {
        timeArived = 0;
        Application.runInBackground = true;
        netListener = new EventBasedNetListener();
        netPacketProcessor = new NetPacketProcessor(); // Same thing to initalize as the server
        netPacketProcessor.RegisterNestedType(Vector3Utils.Serialize, Vector3Utils.Deserialize);
        netListener.PeerConnectedEvent += (server) =>
        {
            
            //Debug.LogError($"Connected to server: {server}");
        };
        netListener.NetworkReceiveEvent += (server, reader, deliveryMethod) => {
           
            netPacketProcessor.ReadAllPackets(reader, server);
            reader.Recycle();
            //Debug.Log("recived");
        };

        netPacketProcessor.SubscribeReusable<NetworkFacePacket>((packet) =>
        {

            Debug.Log(packet.LastFrameTime);
            if (lastUpdate != null )
            {
               
                 
                    LastTimeArived = timeArived;
                    timeArived = Time.time;
                
            }
            else
                timeArived = Time.time; 
            lastUpdate = packet;
        });

            netManager = new NetManager(netListener);
    }

    private void Update()
    {
        netManager.PollEvents();
        
    }
    public void ConnextToServer(String ipAdress)
    {
        ip = ipAdress;
        netManager.Start(); // Don't forget to call .Start()!
        netManager.Connect(ip, 9050,"");
        

    }
}