using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//TODO: expand blend pairs to handle many blend groups.
public class PacketReader : MonoBehaviour
{
    [SerializeField] AnimeMover animegirl;
    [SerializeField] camSwitch camSwitch;
    [SerializeField] Client client;
    [SerializeField] bool bothEyes;
    Quaternion startrot;
    [SerializeField] myLive2dTest testing;
    [SerializeField]AnimationCurve browCurve;
    [SerializeField] AnimationCurve faceCurve;
    [SerializeField] AnimationCurve faceXCurve;
    float[] values;

    float timeDilationValue;
    // Start is called before the first frame update
    void Start()
    {
        
        values = new float[5];
        startrot = animegirl.Neck.rotation;   
    }

    // Update is called once per frame
    void Update()
    {
        if (client.lastUpdate != null)
        {
            Debug.Log(client.time);
            NetworkFacePacket packet = client.lastUpdate;
           camSwitch.timeDilation = (client.time / Time.deltaTime);
            if (camSwitch.timeDilation < 1)
               camSwitch.timeDilation = 1;
            animegirl.head.rotation = Quaternion.RotateTowards(animegirl.head.rotation, Quaternion.Euler((-packet.HeadRotatonEulerAngles.x),
                -packet.HeadRotatonEulerAngles.y, packet.HeadRotatonEulerAngles.z),Time.deltaTime*100f);
            animegirl.Neck.rotation = Quaternion.Slerp(startrot, Quaternion.Euler(startrot.eulerAngles.x, -packet.HeadRotatonEulerAngles.y, 
                startrot.eulerAngles.z), 0.25f);
         

            
            animegirl.root.transform.position= animegirl.startPos + new  Vector3(0,packet.headPos.y/2,0);
            if (camSwitch.Calibrated)
            {

                animegirl.mouthO.amount = faceCurve.Evaluate(camSwitch.mouthSounds.O.Ysim(packet.MouthPoints));
             
                animegirl.mouthI.amount = faceXCurve.Evaluate(camSwitch.mouthSounds.O.Xsim(packet.MouthPoints));


                testing.ModelUpdate(packet,camSwitch);
              
                animegirl.RightBrow.amount = animegirl.LeftBrow.amount = browCurve.Evaluate(Mathf.Lerp(100, 0, Mathf.InverseLerp(camSwitch.LbrowC, camSwitch.LbrowO, 
                    Vector3.Distance(packet.CHIN, packet.Head))));
                

            }
            
        }
    }


    int findhighestIndex(float[] e )
    {
        int i = 0;
        float x = 0;
        for(int z= 0;z<e.Length;z++)
        {
            if (e[z] > x)
            {
                x = e[z];
                i = z;
            }
        }
        return i;
    }

}
