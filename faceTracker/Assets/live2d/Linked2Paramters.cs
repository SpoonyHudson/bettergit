using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Live2D.Cubism.Core;
using Live2D.Cubism.Rendering;
//Link 2 or more paramters together 
[System.Serializable]
public class Linked2Paramters 
{
    [SerializeField] public string MainParamID;
    private int index;
    public float cap;
    public float min;
    CubismModel model;
    public Live2dRigSet[] linkedParams;
    public void setup(CubismModel c)
    {
        model = c;
        for (int i = 0; i < model.Parameters.Length; i++)
        {
            if (model.Parameters[i].Id == MainParamID)
            {
                index = i;
                break;
            }
        }
        cap = model.Parameters[index].MaximumValue;
        min = model.Parameters[index].MinimumValue;
        foreach (Live2dRigSet r in linkedParams)
            r.setup(model);
    }
    public void update(float timeDilation)
    {
        foreach (Live2dRigSet R in linkedParams)
            {
            R.ChangeValue( Mathf.Lerp(R.min,R.cap,Mathf.InverseLerp(min,cap, model.Parameters[index].Value)),1);
            }
    }
}
