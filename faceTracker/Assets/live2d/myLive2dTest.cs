using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Live2D.Cubism.Core;
using Live2D.Cubism.Rendering;
using UnityEngine.UI;
public class myLive2dTest : MonoBehaviour
{
    [SerializeField] CubismModel model;
    [SerializeField] Live2dRigSet[] Rigs;

    [SerializeField] Linked2Paramters[] extraParams;
    float i;
    // Start is called before the first frame update
    void Start()
    {
        
        i = 0;
        foreach (Live2dRigSet r in Rigs)
            r.setup(model);
        foreach (Linked2Paramters p in extraParams)
            p.setup(model);
    }

    // Update is called once per frame
    void Update()
    {

        model.ForceUpdateNow();
    }

    

   public void ModelUpdate(NetworkFacePacket p,camSwitch camSwitch)
    {
        
        foreach (Live2dRigSet r in Rigs)
            r.update(p,camSwitch);
        foreach (Linked2Paramters param in extraParams)
        { // param.update(1);
        }
      
    }
    
   public void setHeadroations(Vector3 rotaion,float XSim,float Ysim)
    {

     
       

        //-3dy to head x
        //-3dx to head y
        //-3dz to head z
    }

    int GetIndex(Live2dRigSet set)
    {
        for (int i = 0; i < model.Parameters.Length; i++)
        {
            if (model.Parameters[i].Id == set.ParamID)
                return i;
        }
        return 0;
    }
}
