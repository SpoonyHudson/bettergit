using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Live2D.Cubism.Core;
using Live2D.Cubism.Rendering;

[System.Serializable]
public class Live2dRigSet
{
    
    [SerializeField] public string ParamID; //The name of the parameter 
    [HideInInspector] public int index; //The real index number of the parameter
   [HideInInspector] public float cap;
    [HideInInspector] public float min;
      float _amount;
    [HideInInspector] public float curveMIN;
    [HideInInspector] public float curveRange;
    public float Multiplyer;
    CubismModel model;
    public PacketData packetData;
    public AmountType amountType;

    public float ScaledAmount { 
        get { return _amount; }
        set{
            Debug.Log(value.ToString() +" adjusted:"+ (((value - (curveMIN)) * (cap - min) / curveRange) + min).ToString());
            model.Parameters[index].Value= _amount = ((value - (curveMIN)) * (cap - min) / curveRange) + min;
        }
    }

    public float amount { 
        get { return _amount; }
        set
        {
            //if (value > cap)
            //    value = cap;
            //if (value < min)
            //    value = min;
            model.Parameters[index].Value = _amount = value;
        }
    }

    public float loopingAmount
    {
        get { return _amount; }
        set {
            float i = value;
            if (i > cap)
            { 
                i-=cap;
            }
            else if (i < min)
            {  
                i+= min;
            }
            model.Parameters[index].Value = _amount = i;
        }
    }

    public void setup(CubismModel c)
    {
        model = c;
        for (int i = 0; i < model.Parameters.Length; i++)
        {
            if (model.Parameters[i].Id == ParamID)
            {
                index = i;
                break;
            }
        }
        cap = model.Parameters[index].MaximumValue;
        min = model.Parameters[index].MinimumValue;
      

    }

    float resetRotScale(float value)
    {
        if (value > 180)
        {
            return (value - 360);
        }
        else
            return value;
    }

    public void ChangeValue(float v,float timedilation)
    {
        v *= Multiplyer;
        v = Mathf.MoveTowards(amount, v, Mathf.Abs(v - amount) / timedilation);

        switch (amountType)
        {
            case AmountType.normal:
                {
                    amount = v;
                    break;
                }
            case AmountType.loop:
                {
                    loopingAmount = v;
                    break;
                }
            case AmountType.scaled:
                {
                    ScaledAmount = v;
                    break;
                }
            default:
                {
                    break;
                }
        }    
    }

    public void update(NetworkFacePacket p, camSwitch camSwitch)
    {


        switch (packetData)
        {
            case PacketData.HeadRotationX:
            {
                   float v= resetRotScale(p.HeadRotatonEulerAngles.x);
                    ChangeValue(v,camSwitch.timeDilation);
                   break;
            }
            case PacketData.HeadRotationY:
                {
                    float v = resetRotScale(p.HeadRotatonEulerAngles.y);
                    ChangeValue(v,camSwitch.timeDilation);
                    break;
                }
            case PacketData.HeadRotationZ:
                {
                    float v = resetRotScale(p.HeadRotatonEulerAngles.z);
                    ChangeValue(v,camSwitch.timeDilation);
                    break;
                }

            case PacketData.MouthX:
                {
                   
                    ChangeValue(camSwitch.mouthSounds.O.Xsim(p.MouthPoints),camSwitch.timeDilation);
                    break;
                }
            case PacketData.MouthY:
                {

                    ChangeValue(camSwitch.mouthSounds.O.Ysim(p.MouthPoints),camSwitch.timeDilation);
                    break;
                }
            case PacketData.HeadLength:
            {
                    ChangeValue(Vector3.Distance(p.CHIN, p.BrowR),camSwitch.timeDilation);
                    break;
            }
            case PacketData.mouseX:
            {
                    ChangeValue(Input.mousePosition.x, 1);
                    break;
            }

            case PacketData.MouseY:
            {
                    ChangeValue(Input.mousePosition.y, 1);
                    break;
            }
            default:
            {
                break;
            }
        
        }
    }

}


public enum AmountType
{
    normal,scaled,loop
}
public enum PacketData
{
    MouthX,MouthY,HeadLength,HeadRotationX,HeadRotationY,HeadRotationZ,mouseX,MouseY
}
